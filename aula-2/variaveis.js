// let caixinha = 10 % 2
// let e_par = caixinha === 0
// let e_impar = !e_par
// let texto = "hello world";

// outros jeitos de concatenar strings
// console.log("o valor da caixinha é: " + caixinha)
// console.log('o valor da caixinha é: ' + caixinha)
// console.log(`o valor da caixinha é: ${caixinha} e é par? ${e_par}`)


// console.log(caixinha)
// console.log(e_par)
// console.log(e_impar)

// caixinha = 11 % 2;
// const caixinha_constante = caixinha;

// console.log("A" == "B") // false
// console.log("A" != "B") // true
// console.log("10" == 10) // true
// console.log("10" === 10) // false
// console.log(typeof "A" == typeof "B") // true



let number = 10;
let texto = "string";
let boleanas = true;
let objeto = {
  prop1: 10,
  prop2: "texto"
}
let lista_de_numeros = [1, 2, 3];
let lista_de_strings = ["um", "dois", "tres"];
let lista_de_objetos = [
  {
    numero: 1,
    extenso: "um"
  },
  {
    numero: 2,
    extenso: "dois"
  },
  {
    numero: 3,
    extenso: "tres OBJ"
  }
];
let lista_de_lista = [
  lista_de_numeros,
  lista_de_strings,
  lista_de_objetos
]
// console.log(lista_de_lista) // [ [ 1, 2, 3 ], [ 'um', 'dois', 'tres' ] ]
console.log(lista_de_lista[0][2])
console.log(lista_de_lista[1][2])
console.log(lista_de_lista[2][2].numero) 