/*
Como fazer um programa que calcule médias e médias ponderadas,
de X notas a partir das entradas do usuário?
 */

let nota_1 = 20;
let nota_2 = 80;
let nota_3 = 50;

let media = (nota_1 + nota_2 + nota_3) / 3;
let ponderada = (nota_1 * 1 + nota_2 * 1.5 + nota_3 * 1) / 3.5;

console.log(`A média é ${media}`)
console.log(`A ponderada é ${ponderada}`)


let notas = [];
notas[0] = 20;
notas[1] = 50;
notas[2] = 50;


console.log(notas[3])
let media_com_arrays = (notas[0] + notas[1] + notas[2]) / 3;
let ponderada_com_arrays = (notas[0] * 1 + notas[1] * 1.5 + notas[2] * 1) / 3.5;

console.log("Com arrays:")
console.log(`A média é ${media_com_arrays}`)
console.log(`A ponderada é ${ponderada_com_arrays.toFixed(2)}`)



let notas = [];
// Nota da Ana
notas[0] = [9, 2, 10];

// Nota da Bruna
notas[1] = [5, 5, 5];

// Nota da Carol
notas[2] = [0, 10, 10];

console.log(notas)

let notas_estrutura = {
  ana: [9, 2, 10],
  bruna: [5, 5, 5],
  carol: [0, 10, 10]
};

let aluno = {
  nome: "Ana",
  notas: [9, 2, 10]
}

let alunos = [
  {
    nome: "Ana",
    notas: [9, 2, 10]
  },
  {
    nome: "Bruna",
    notas: [9, 2, 10]
  },
  {
    nome: "Carol",
    notas: [0, 10, 10]
  },
]

console.log(alunos[0].nome)