/*

Resolva o famoso exercício "FizzBuzz"

Para multiplos de 3 deverá ser impresso a palavra “fizz” em uma nova linha;
Para multiplos de 5 deverá ser impresso a palavra “buzz” em uma nova linha;
Para multiplos de 3 e 5, deverá ser impresso a palavra “fizzbuzz” em uma nova linha;

fizzbuzz
1
2
fizz
4
buzz
fizz
7
8
fizz
buzz
11
fizz
13
14
fizzbuzz
*/

for (let contador = 0; contador < 61; contador++) {
  if (contador % 3 === 0 && contador % 5 === 0 && contador % 20 === 0) {
    console.log("fizzbuzz");
  }
  else if (contador % 3 === 0) {
    console.log("fizz");
  }
  else if (contador % 5 === 0) {
    console.log("buzz");
  }
  else {
    console.log(contador);
  }
}


for (let contador = 0; contador < 100; contador++) {
  let resultado = "";
  if (contador % 3 === 0) resultado += "fizz";
  if (contador % 5 === 0) resultado += "buzz";
  if (contador % 20 === 0) resultado += "ABACAXI";
  if (resultado === "") resultado = contador;
  console.log(resultado);
}