let notas = [
  8.5,
  9.9,
  5,
  2,
  10,
  5.5,
  6,
  9,
]

const media = 6

for (let index = 0; index < notas.length; index++) {
  const nota = notas[index];

  if (nota > 7)
    console.log(`nota = ${nota} Parabéns, você está aprovado!`)
  else if (nota > 5.5)
    console.log(`nota = ${nota} Você não fez mais que sua obrigação!`)
  else
    console.log(`nota = ${nota} Você está REPROVADO!!`)
}